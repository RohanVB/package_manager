import unittest

from sarge import get_both

from dependency_management.requirements.AtomRequirement import AtomRequirement


class AtomRequirementTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        import pip
        del pip

    def test__str__(self):
        self.assertEqual(str(AtomRequirement('coala')), 'coala')
        self.assertEqual(str(AtomRequirement('coala', '0.3.0')),
                         'coala 0.3.0')

    def test_install_command_with_version(self):
        self.assertEqual(
            ['apm', 'install', 'coala@0.3.0'],
            AtomRequirement('coala', '0.3.0').install_command())

    def test_install_command_without_version(self):
        self.assertEqual(['apm', 'install', 'coala'],
                         AtomRequirement('coala').install_command())

    def test_installed_requirement(self):
        pass
        # self.assertTrue(AtomRequirement('pip').is_installed())

    def test_not_installed_requirement(self):
        self.assertFalse(AtomRequirement('some_bad_package').is_installed())


if __name__ == '__main__':
    unittest.main()
