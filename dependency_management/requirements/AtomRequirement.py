import sys
from sarge import Capture
from sarge import get_stdout
from sarge import run

from dependency_management.requirements.ExecutableRequirement import (
    ExecutableRequirement)
from dependency_management.requirements.PackageRequirement import (
    PackageRequirement)


class AtomRequirement(PackageRequirement):
    """
    This class is a subclass of ``PackageRequirement``. It specifies the proper
    type for ``python`` packages automatically and provide a function to check
    for the requirement.
    """

    REQUIREMENTS = {ExecutableRequirement(sys.executable)}

    def __init__(self, package, version=""):
        """
        Constructs a new ``AtomRequirement``, using the ``AtomRequirement``
        constructor.

        >>> pr = AtomRequirement('coala', '0.3.0')
        >>> pr.type
        'apm'
        >>> pr.package
        'coala'
        >>> pr.version
        '0.3.0'
        >>> str(pr)
        'coala 0.3.0'

        :param package: A string with the name of the package to be installed.
        :param version: A version string. Leave empty to specify latest version.
        """
        PackageRequirement.__init__(self, 'apm', package, version)

    def install_command(self):
        """
        Creates the installation command for the instance of the class.

        :param return: A list with the installation command parameters.
        """
        result = ['apm', 'install',
                  self.package + '@' + self.version if self.version
                  else self.package]
        return result

    def is_installed(self):  # todo
        """
        Checks if the dependency is installed.

        :param return: True if dependency is installed, false otherwise.
        """

    def upgrade_package(self):
        """
        Runs the upgrade command for the package given in a sub-process.
        """
        run('apm upgrade ' + self.package,
            stdout=Capture(),
            stderr=Capture())

    def uninstall_package(self):
        """
        Runs the uninstall command for the package given in a sub-process.
        """
        run('apm uninstall ' + self.package,
            stdout=Capture(),
            stderr=Capture())
        self.is_installed.cache_clear()
